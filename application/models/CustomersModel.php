<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class UsersModel
 */
class CustomersModel extends CI_Model{
    const TABLE_NAME            = 'customers';
    const FIELD_ID              = 'id';
    const FIELD_NAME            = 'name';
    const FIELD_CNP             = 'cnp';

    public function __construct() {
        parent::__construct();
    }

    /**
     * Create entity Customer
     * @param string $name
     * @param string $cnp
     * @return int
     * @throws Exception
     */
    public function create($name, $cnp) {
        if (!$name || !$cnp) {
            throw new Exception('Params are not correct');
        }

        $result = $this->db->insert(self::TABLE_NAME,  [
            self::FIELD_NAME  => $name,
            self::FIELD_CNP   => $cnp
        ]);

        if (!$result || !$id = $this->db->insert_id()) {
            throw new Exception('Operation was failed');
        }

        return $id;

    }

}