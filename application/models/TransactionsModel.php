<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class UsersModel
 */
class TransactionsModel extends CI_Model{
    const TABLE_NAME            = 'transactions';
    const FIELD_ID              = 'id';
    const FIELD_CUSTOMER_ID     = 'customer_id';
    const FIELD_AMOUNT          = 'amount';
    const FIELD_DATE            = 'date';

    public function __construct() {
        parent::__construct();
    }

    public function getCountByCid ($cid) {
        if (!$cid) {
            throw new Exception('Params are not correct');
        }

        $this->db->where(self::FIELD_CUSTOMER_ID, $cid);
        return $this->db->get(self::TABLE_NAME)->num_rows();
    }

    public function getByCid ($cid, array $params) {
        if (!$cid) {
            throw new Exception('Params are not correct');
        }

         $this->db->where(self::FIELD_CUSTOMER_ID, $cid);

         if (isset($params['offset']) && isset($params['limit'])) {
            $this->db->offset($params['offset']);
            $this->db->limit($params['limit']);
         }

        if (isset($params['amount']) && $this->setAmount($params['amount'])) {
            $int = $this->setAmount($params['amount']);
            $this->db->where(self::FIELD_AMOUNT, $int);
        }

        $result = $this->db->get(self::TABLE_NAME)->result_array();
        $transactions = [];

        foreach ($result as $item) {
            $transactions[] = [
                self::FIELD_ID          => $item[self::FIELD_ID],
                self::FIELD_CUSTOMER_ID => $item[self::FIELD_CUSTOMER_ID],
                self::FIELD_DATE        => $item[self::FIELD_DATE],
                self::FIELD_AMOUNT      => $this->getAmount($item[self::FIELD_AMOUNT]),
            ];
        }

         return $transactions;
    }

    public function getOne ($cid, $tid) {
        if (!$cid || !$tid) {
            throw new Exception('Params are not correct');
        }
        return $this->db->where(self::FIELD_ID, $tid)->where(self::FIELD_CUSTOMER_ID, $cid)->get(self::TABLE_NAME)->row_array();
    }

    public function create($cid, $amount) {
        if (!$cid || !$amount) {
            throw new Exception('Params are not correct');
        }

        $result = $this->db->insert(self::TABLE_NAME,  [
            self::FIELD_CUSTOMER_ID  => $cid,
            self::FIELD_AMOUNT       => $this->setAmount($amount),
            self::FIELD_DATE         => date("Y-m-d H:i:s", time())
        ]);

        if (!$result || !$id = $this->db->insert_id()) {
            throw new Exception('Operation was failed');
        }

        return $id;

    }

    /**
     * Remove entity
     * @param $tid Transaction ID
      * @throws Exception
     */
    public function delete ($tid) {
        if (!$tid) {
            throw new Exception('Params are not correct');
        }

        if (!$this->db->delete(self::TABLE_NAME, [self::FIELD_ID  => $tid] )) {
            throw new Exception('Operation was failed');
        }

    }

    /**
     * @param $tid
     * @param array $params
     * @throws Exception
     */
    public function update ($tid, array $params) {
        if (!$tid) {
            throw new Exception('Params are not correct');
        }
        $this->db->where(self::FIELD_ID, $tid);
        if (!$this->db->update(self::TABLE_NAME, [
            'amount' => $this->setAmount($params['amount'])
        ] )) {
            throw new Exception('Operation was failed');
        }

    }


    /**
     * Parse string and return amount as a string
     * @param $string
     * @return float|null
     */
    private function setAmount ($string) {
        $float = (float) str_replace(',', '.', $string);
        if ($float) {
            return $float * 100;
        }
        return null;
    }

    private function getAmount ($int) {
        return $int / 100;
    }

    public function updateStatistic() {
        // ...set to DB statistic
    }

}