<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class UsersModel
 */
class UsersModel extends CI_Model{
    const TABLE_NAME            = 'users';
    const FIELD_ID              = 'id';
    const FIELD_USERNAME        = 'username';
    const FIELD_PASSWORD        = 'password';
    const FIELD_SALT            = 'salt';
    const FIELD_EMAIL           = 'email';
    const FIELD_ACTIVATION_CODE = 'activation_code';

    const USER_TMP_ANONYMOUS = 1;

    public function __construct() {
        parent::__construct();
    }

    /**
     * @param int|null $apUserId
     * @return Array|Boolean
     */
    public function create($apUserId=null) {
        $accountName = $this->getNewUserName();

        try {
            $this->db->trans_start();

            $data = [
                self::FIELD_AP_USER_ID     => (int) ( $apUserId ? $apUserId : Ap_users::USER_TMP_ANONYMOUS),
                self::FIELD_DU_USERNAME    => $accountName,
                self::FIELD_CREATED_AT     => mdate('%Y-%m-%d %H:%i:%s', now())
            ];
            $result = $this->db->insert(self::TABLE_NAME,  $data);
            if (!$result) {
                throw new Exception('Isert operation was failed');
            }

            $apiCallResult = self::$dudaApi->createAccount(
                $accountName,
                self::FIRST_NAME_DEF,
                self::LAST_NAME_DEF,
                self::EMAIL_DEF,
                self::ACCOUNT_TYPE_CUSTOMER
            );

            $this->db->trans_complete();

            return $data;
        } catch (Exception $e) {
            $this->db->trans_rollback();

            return false;
        }
    }

}