<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Migrate
 * @property TransactionsModel $TransactionsModel
 */
class Cron extends CI_Controller
{
	public function index()
	{
		show_404();
	}

	/**
	 * Transactions SUM
	 * */
	// 47 23 */2 * * php /your_path_to_app/index.php cron calc_sum 1 >> /your_path_to_app/application/logs/cron.log 2>&1
	// for test run cron use http://backend_test.local/cron/calc_sum

	public function calc_sum()
	{
		$this->load->model('TransactionsModel');
		$this->TransactionsModel->updateStatistic();
		return true;
	}

}