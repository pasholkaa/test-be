<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Migrate
 * @property CI_Migration $migration
 */
class Migrate extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('migration');

	}

	public function index()
	{
		if ($this->migration->current() === FALSE)
		{
			show_error($this->migration->error_string());
		}
	}

	/**
	 * @todo Need be more secure
	 */
	public function up () {
		if ($this->migration->latest() === FALSE)
		{
			show_error($this->migration->error_string());
		}
		echo 'The DB is up to date';
	}

//	public function down () {
//
//	}

}