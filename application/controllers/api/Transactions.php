<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 * @property CustomersModel $CustomersModel
 * @property TransactionsModel $TransactionsModel
 */
class Transactions extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['transactions_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['transactions_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['transactions_delete']['limit'] = 50; // 50 requests per hour per user/key
        $this->methods['transactions_put']['limit'] = 50; // 50 requests per hour per user/key

        $this->load->model('TransactionsModel');
    }

    public function transactions_get($customerId)
    {
        $cid            = filter_var($customerId, FILTER_SANITIZE_NUMBER_INT);
        $offset         = (int) $this->get('offset');
        $limit          = (int) $this->get('limit');
        $date           = $this->get('date');
        $amount         = $this->get('amount');

        if (!$cid) {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST);
        }

        try {
            $result = $this->TransactionsModel->getByCid($customerId, [
                'offset' => $offset,
                'limit'  => $limit,
                'date'   => $date,
                'amount' => $amount
            ]);

            $countRecords = $this->TransactionsModel->getCountByCid($customerId);

            $responce = [
                'recordsTotal'      => $countRecords,
                'recordsFiltered'   => $countRecords,
                'data'              => $result,
                'params'            => [
                    'offset' => $offset,
                    'limit'  => $limit
                ]
            ];

            $this->set_response($responce, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        } catch (Exception $e) {
            $this->set_response('Server error', REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
        }
    }


    public function transaction_get($customerId, $transactionId)
    {
        $cid = filter_var($customerId, FILTER_SANITIZE_NUMBER_INT);
        $tid = filter_var($transactionId, FILTER_SANITIZE_NUMBER_INT);

        if (!$cid || !$tid) {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST);
        }

        try {
            $result = $this->TransactionsModel->getOne($cid, $tid);

            if (!$result) {
                $this->set_response(NULL, REST_Controller::HTTP_NOT_FOUND); // OK (200) being the HTTP response code
                return;
            }

            $this->set_response($result, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        } catch (Exception $e) {
            $this->set_response('Server error', REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function transactions_post()
    {
        $cid        = filter_var($this->post('customerId'), FILTER_SANITIZE_NUMBER_INT);
        $amount     = filter_var($this->post('amount'),     FILTER_SANITIZE_STRING);

        if (!$cid || !$amount ) {
            $this->set_response('Param is not correct', REST_Controller::HTTP_BAD_REQUEST);
        }

        try {
            $id = $this->TransactionsModel->create($cid, $amount);

            $message = [
                'id' => (int) $id
            ];

            $this->set_response($message, REST_Controller::HTTP_CREATED);
        } catch (Exception $e) {
            $this->set_response('Server error', REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
        }

    }

    public function transactions_delete($id)
    {
        $id = (int) $id;

        if ($id <= 0)
        {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST);
        }

        try {
            $this->TransactionsModel->delete($id);
            $message = [
                'id' => $id,
                'message' => 'Deleted the resource'
            ];

            $this->set_response($message, REST_Controller::HTTP_NO_CONTENT); // NO_CONTENT (204) being the HTTP response code
        } catch(Exception $e) {
            $this->response(NULL, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function transactions_put($id)
    {
        $id = (int) $id;
        $amount  = filter_var($this->put('amount'), FILTER_SANITIZE_STRING);

        if ($id <= 0)
        {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST);
        }

        try {
            $this->TransactionsModel->update($id, ['amount' => $amount ]);

            $message = [
                'id' => $id,
                'message' => 'Updated the resource'
            ];

            $this->set_response($message, REST_Controller::HTTP_NO_CONTENT); // NO_CONTENT (204) being the HTTP response code
        } catch(Exception $e) {
            $this->response(NULL, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

}
