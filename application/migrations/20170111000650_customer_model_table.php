<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_customer_model_table extends CI_Migration
{
    const TABLE_NAME = 'customers';

    public function up()
    {
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 20,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'cnp' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => FALSE,
            ),
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => FALSE,
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table(self::TABLE_NAME);

    }

    public function down()
    {
        $this->db->truncate(self::TABLE_NAME);
        $this->dbforge->drop_table(self::TABLE_NAME);
    }
}