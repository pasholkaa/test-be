<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_transactions_model_table extends CI_Migration
{
    const TABLE_NAME            = 'transactions';
    const FIELD_CUSTOMER_ID     = 'customer_id';
    const FK_CUSTOMER_ID        = "FK_CUSTOMER_ID";

    public function up()
    {
        $tableName = self::TABLE_NAME;

        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 20,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            self::FIELD_CUSTOMER_ID => array(
                'type' => 'INT',
                'constraint' => '20',
                'unsigned' => TRUE,
                'null' => FALSE,
            ),
            'amount' => array(
                'type' => 'INT',
                'constraint' => '20',
                'null' => FALSE,
            ),
            'date' => array(
                'type' => 'TIMESTAMP',
                'null' => TRUE,
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table(self::TABLE_NAME);

        $index     = "ALTER TABLE `$tableName` ADD INDEX(`" . self::FIELD_CUSTOMER_ID . "`)";
        $FK        = "ALTER TABLE `$tableName` ADD CONSTRAINT `". self::FK_CUSTOMER_ID."` FOREIGN KEY (`" . self::FIELD_CUSTOMER_ID . "`) REFERENCES `api`.`customers`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;";

        $this->db->query($index);
        $this->db->query($FK);

    }

    public function down()
    {
        $tableName = self::TABLE_NAME;
        $this->db->query("ALTER TABLE `$tableName` DROP FOREIGN KEY `" . self::FK_CUSTOMER_ID. "`;");
        $this->db->truncate(self::TABLE_NAME);
        $this->dbforge->drop_table(self::TABLE_NAME);
    }
}