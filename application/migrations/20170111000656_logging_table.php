<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_logging_table extends CI_Migration
{
    const TABLE_NAME            = 'logs';
    public function up()
    {
        $tableName = self::TABLE_NAME;
        $sql = '
              CREATE TABLE `logs` (
                  `id` INT(11) NOT NULL AUTO_INCREMENT,
                  `uri` VARCHAR(255) NOT NULL,
                  `method` VARCHAR(6) NOT NULL,
                  `params` TEXT DEFAULT NULL,
                  `api_key` VARCHAR(40) NOT NULL,
                  `ip_address` VARCHAR(45) NOT NULL,
                  `time` INT(11) NOT NULL,
                 `rtime` FLOAT DEFAULT NULL,
                  `authorized` VARCHAR(1) NOT NULL,
                  `response_code` smallint(3) DEFAULT \'0\',
                   PRIMARY KEY (`id`)
              ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
               ';
        $this->db->query($sql);


    }

    public function down()
    {
        $this->db->truncate(self::TABLE_NAME);
        $this->dbforge->drop_table(self::TABLE_NAME);
    }
}