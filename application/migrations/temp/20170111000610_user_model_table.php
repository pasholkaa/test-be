<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_user_model_table extends CI_Migration
{
    const TABLE_NAME = 'users';

    public function up()
    {
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 20,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'email' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => FALSE,
            ),
            'username' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => FALSE,
            ),
            'password' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'null' => FALSE,
            ),
            'salt' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'null' => TRUE,
            ),
            'activation_code' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'null' => TRUE,
            ),
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table(self::TABLE_NAME);

        $this->db->insert('ap_users', [
            'email'     => 'admin',
            'username'  => 'tmpanonimus',
            'password'  => 'admin',
        ]);

    }

    public function down()
    {
        $this->db->truncate(self::TABLE_NAME);
        $this->dbforge->drop_table(self::TABLE_NAME);
    }
}