<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_keys_table extends CI_Migration
{
    const TABLE_NAME            = 'keys';
    public function up()
    {
        $tableName = self::TABLE_NAME;
        $sql = '  CREATE TABLE `keys` (
                   `id` INT(11) NOT NULL AUTO_INCREMENT,
                   `user_id` INT(11) NOT NULL,
                   `key` VARCHAR(40) NOT NULL,
                   `level` INT(2) NOT NULL,
                   `ignore_limits` TINYINT(1) NOT NULL DEFAULT \'0\',
                   `is_private_key` TINYINT(1)  NOT NULL DEFAULT \'0\',
                   `ip_addresses` TEXT NULL DEFAULT NULL,
                   `date_created` INT(11) NOT NULL,
                   PRIMARY KEY (`id`)
               ) ENGINE=InnoDB DEFAULT CHARSET=utf8;';
        $this->db->query($sql);


    }

    public function down()
    {
        $this->db->truncate(self::TABLE_NAME);
        $this->dbforge->drop_table(self::TABLE_NAME);
    }
}