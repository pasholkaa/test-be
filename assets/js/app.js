var table = null;
$(document).ready(function() {
    table = $('#datatable').DataTable({
        columns: [
            {data: "id", 			orderable: false},
            {data: "customer_id", 	orderable: false},
            {data: "amount", 		orderable: false},
            {data: "date", 			orderable: false}
        ],
        processing: true,
        serverSide: true,
//					lengthMenu: [[5, 10, 50], [5, 10, 50]],
        ajax: {
            url: '/api/transactions/1',
            type: 'GET',
//						'headers': {
//							'Authorization': ''
//						},
            data: function(d) {
                console.log(d);
                var data = {
                    offset: d.start,
                    limit: d.length
                };

                if (d.columns[3].search.value) {
                    data.date = d.columns[3].search.value;
                }
                if (d.columns[2].search.value) {
                    data.amount = d.columns[2].search.value;
                }
                if (d.columns[1].search.value) {
                    data.customer_id = d.columns[1].search.value;
                }
//							d = JSON.stringify(data);
                d = data;
                return d;
            }
        }
    });
});


$(document).ready(function() {
    $('.date-picker').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        autoUpdateInput: false,
        locale: {
            format: 'YYYY-MM-DD'
        }
    }, function (chosen_date) {
        $(this.element).val(chosen_date.format('YYYY-MM-DD')).trigger('change');
    });
});

function search(el, column, event) {
    $(el).on(event, function (e) {
        table.column(column).search(this.value).draw();
        /*            if (this.value == "")
         table.column(column).search("").draw();*/

    });
}

search('#amount', 2, 'keyup');
search('#date', 3,   'change');
search('#customer_id', 1, 'keyup');