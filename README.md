# REST API, based on CodeIgniter framework

##Screenshots:
 **[Login page](http://prntscr.com/e9yvat)**  
 **[Dashboard page](http://prntscr.com/e9ywxe)**   
 **[Dashboard page with filter](http://prntscr.com/e9z4dr478558)** 
 
## Requirements
1. PHP 5.6+
2. Memcached 2.2+

**Install dependencies**
```php
composer  install
```
Note: If you don't want use composer for installing dependencies via manager, please unzip archive "vendor.zip in the root directory"

## Database
**Create DB and User for DB**
```
CREATE database api DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;

CREATE USER 'api_user'@'localhost' identified BY '0e9M2vxfGiY16lJR';
GRANT ALL ON `api`.* TO 'api_user'@'localhost';

FLUSH PRIVILEGES;
```

**Fill DB**
You can do it in two ways.
1. Import by SQL DB dump. Please, find the dump file in application/DB/db_dump.sql
2. Run the DB migrations automatically. 
```
http://<localhost|your domain>/migrate
```
You can find migration directory application/migrations


## Configuration for application
Please find configuration file and specify the domain
application/config/config.php
```
$config['base_url'] = '<domain>'; // Ex: http://api.dev/
```

## Username and passwor for access to dashboard

Username: admin@admin.com  
Password: password

##API DOC

**Create customer**
POST  http://api.dev/api/customers  
Format: x-www-form-urlencoded  
Required Parameters:  
 name : "John"  
 cnp  : "788888787"   
 
Expected Return:  
 Http Status Code: 200   
 JSON:   
 ```
 {
   "id": 1
 }
 ```
   
 **Create transaction**
 POST  http://api.dev/api/transactions  
 Format: x-www-form-urlencoded   
 Required Parameters:  
  customerId : 1  
  amount   : "289.98"   
  
 Expected Return:  
  Http Status Code: 200  
  JSON:  
  ```
  {
    "id": 1
  }
  ```
   
 **Get transaction**
 GET  http://api.dev/api/transaction/{customerId}/{transactionId}  
  Expected Return:  
  Http Status Code: 200  
  JSON:  
  ```
  {
    "transactionId"​ :​  ​ 100, 
    "amount"​ :​  ​ 205.67,
    "date"​ :​  ​ "20.03.2015
  }
  ```  
   
 **Get transactions by customer**
   GET  http://api.dev/api/transaction/{customerId}}  
    Expected Return:  
    Http Status Code: 200 
    JSON:  
    ```
    [
        {
          "transactionId"​ :​  ​ 100, 
          "amount"​ :​  ​ 205.67,
          "date"​ :​  ​ "20.03.2015
        },
        {
           "transactionId"​ :​  ​ 1, 
           "amount"​ :​  ​ 205.67,
           "date"​ :​  ​ "20.03.2015
         }
    ]
    ```  
    
**Delete transactions** 
DELETE  http://api.dev/api/transactions/TRANSACTION_ID  
Expected Return:  
 Http Status Code: 204  
 JSON:  
 ```
 {
    'id' => 1,
    "message" => "Deleted the resource"
 }
 ```
   
**Update transactions** 
 PUT  http://api.dev/api/transactions/TRANSACTION_ID  
  Required Parameters:  
   amount   : "289.98"  
 Expected Return:  
  Http Status Code: 204  
  JSON:   
  ```
  {
     'id' => 1,
     "message" => "Deleted the resource"
  }
  ```
  ***
  
**Request Logging**  
Each API request will store in DB table "logs"   
[Screenshot](http://prntscr.com/e9z9ga)
 
             
             